USE exemple;

-- CREATE TEMPORARY TABLE -> table temporaire
-- Elle sera supprimée dés que l'on quitte la session
CREATE TEMPORARY TABLE utilisateurs
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(100) NOT NULL,
	prenom VARCHAR(100) NOT NULL,
	email VARCHAR(150) NOT NULL,
	date_naissance DATE
);

SHOW tables;

INSERT INTO utilisateurs(nom,prenom,email,date_naissance) VALUES
('profit','jim','jprofit@dawan.com','1965-09-02');

-- affiche tous les utilisateur
SELECT * FROM utilisateurs ;

-- On se deconnecte du SGBD -> La table n'existe plus

USE bibliotheque;

-- TABLE CTE -> table à usage unique
WITH auteur_vivant_cte AS(
	SELECT id,prenom,nom,naissance FROM auteurs
	WHERE deces IS NULL
) -- La table CTE doit être utilisée immédiatement après sa déclaration, sinon cela provoque une erreur	
SELECT id, prenom, nom, naissance FROM auteur_vivant_cte WHERE naissance >'1950-01-01';
-- erreur
-- SELECT * FROM livres; 
-- SELECT id, prenom, nom, naissance FROM auteur_vivant_cte WHERE naissance >'1950-01-01';

WITH auteur_vivant_cte AS(
	SELECT auteurs.id,prenom,auteurs.nom AS nom_auteur,naissance,pays.nom AS nom_pays FROM auteurs
	INNER JOIN pays ON auteurs.nation =pays.id 
	WHERE deces IS NULL
),
auteur_vivant_usa AS(	-- On peut déclarer plusieurs table CTE à la suite avant de les utiliser
	SELECT prenom, nom_auteur,naissance FROM auteur_vivant_cte WHERE nom_pays='états-unis'
)
SELECT prenom,nom_auteur,naissance  FROM auteur_vivant_usa WHERE naissance>'1950-01-01';

-- Création d'une table à partir d'un SELECT
CREATE TABLE livre_science_fiction 
SELECT livres.id,titre,annee FROM livres
INNER JOIN genres ON genres.id = livres.genre
WHERE genres.nom='science-fiction';

SHOW tables;

SELECT * FROM livre_science_fiction;

DROP TABLE livre_science_fiction;

CREATE TEMPORARY TABLE auteur_france
SELECT auteurs.id,CONCAT_WS(' ',auteurs.prenom,auteurs.nom) AS auteur,
if(deces IS NULL,
round(datediff(current_date() , naissance)/365,0), 
round(datediff(deces  , naissance)/365,0))
AS age ,
deces IS NULL AS est_vivant
FROM auteurs
INNER JOIN pays ON pays.id=auteurs.nation
WHERE pays.nom='france';

SELECT * FROM auteur_france WHERE est_vivant = 0 AND age<60;

-- Exercice table CTE 

USE world;
-- Tous les pays avec une capitale de plus d'un million d'habitant
WITH city_population_million AS 
( 
	SELECT city.id,city.name,city.population FROM city
	INNER JOIN country ON country.capital=city.ID
	WHERE city.population > 1000000
)
SELECT name,population FROM city_population_million ORDER BY population DESC;


-- Liste des pays ayant une langue parlée par moins de 10% de la population
WITH pays_langue_m_10 AS 
(
	SELECT  DISTINCT country.name
	 FROM countrylanguage
	 INNER JOIN country ON country.code=countrylanguage.CountryCode 
	 WHERE countrylanguage.Percentage  < 10 
)
SELECT name FROM pays_langue_m_10;

/* Créer une table temporaire:
 - capitale qui contient
 - id de la ville
 - nom de la ville
 - la population de la ville
 - nom du pays
 - nom du continent
 - la langue officielle la plus parlé dans le pays
*/
-- utiliser cette table temporaire pour afficher les 10 capitales les + peuplés.
SELECT city.id, city.Name , city.Population, country.name,country.Continent,countrylanguage.language FROM city
INNER JOIN country ON country.capital=city.ID 
INNER JOIN countrylanguage  ON country.code =countrylanguage.CountryCode 
WHERE countrylanguage.Percentage=(
	SELECT max(Percentage) FROM countrylanguage c  WHERE CountryCode =country.code
) AND countrylanguage.IsOfficial ='T';
