USE exemple;


-- CHECK permet de limiter la plage de valeurs
CREATE TABLE personnes
(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50),
	nom VARCHAR(50) NOT NULL CHECK(CHAR_LENGTH(nom)>2 AND CHAR_LENGTH(nom)<50) ,
	age INT NOT NULL CHECK(age>=0)
);

INSERT INTO personnes(prenom,nom,age) VALUE ('John','Doe',34);

-- Erreur -> nom < à 3 caractères
INSERT INTO personnes(prenom,nom,age) VALUE ('Jane','Do',33);

-- Erreur -> age négatif
INSERT INTO personnes(prenom,nom,age) VALUE ('Jim','Profit',-4);

INSERT INTO personnes(prenom,nom,age) VALUE ('John','Doe',34),('Jane','Do',33);

-- Gestion des clés étrangère avec  ON DELETE et ON UPDATE
CREATE TABLE marques_cascade(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL,
	date_creation DATE
);

CREATE TABLE articles_cascade(
	reference INT PRIMARY KEY AUTO_INCREMENT,
	description VARCHAR(255),
	prix DECIMAL(6,2) NOT NULL DEFAULT 10.0, 				
	marque INT,
	CONSTRAINT  FK_articles_marques_cascade
	FOREIGN KEY (marque)
	REFERENCES marques_cascade(id)
);

INSERT INTO marques_cascade(nom) VALUES 
('Marque A'),
('Marque B'),
('Marque C');


INSERT INTO articles_cascade(description,prix,marque) VALUES 
('Souris',30.0,1),
('Clavier AZERTY',20.0,1),
('TV 4K',600.0,2),
('Carte mère ',130.0,3),
('Carte graphique',500.0,3);

-- DELETE FROM marques_cascade WHERE id=1;
DELETE FROM articles_cascade  WHERE marque=1;
DELETE FROM marques_cascade WHERE id=1;

SELECT * FROM articles_cascade;
SELECT * FROM marques_cascade;

-- ON DELETE CASCADE
ALTER TABLE articles_cascade DROP CONSTRAINT FK_articles_marques_cascade;

ALTER TABLE articles_cascade ADD CONSTRAINT FK_articles_marques_cascade
FOREIGN KEY(marque)
REFERENCES marques_cascade(id) ON DELETE CASCADE;

INSERT INTO marques_cascade(nom) VALUES ('Marque A');
INSERT INTO articles_cascade(description,prix,marque) VALUES 
('Souris',30.0,4),('Clavier AZERTY',20.0,4);

DELETE FROM marques_cascade WHERE id=4;

-- ON DELETE NULL
ALTER TABLE articles_cascade DROP CONSTRAINT FK_articles_marques_cascade;

ALTER TABLE articles_cascade ADD CONSTRAINT FK_articles_marques_cascade
FOREIGN KEY(marque)
REFERENCES marques_cascade(id) ON DELETE SET NULL;

INSERT INTO marques_cascade(nom) VALUES ('Marque A');
INSERT INTO articles_cascade(description,prix,marque) VALUES 
('Souris',30.0,5),('Clavier AZERTY',20.0,5);

DELETE FROM marques_cascade WHERE id=5;

-- ON UPDATE CASADE
ALTER TABLE articles_cascade DROP CONSTRAINT FK_articles_marques_cascade;

ALTER TABLE articles_cascade ADD CONSTRAINT FK_articles_marques_cascade
FOREIGN KEY(marque)
REFERENCES marques_cascade(id) ON UPDATE CASCADE;

INSERT INTO marques_cascade(nom) VALUES ('Marque A');
INSERT INTO articles_cascade(description,prix,marque) VALUES 
('Souris',30.0,6),('Clavier AZERTY',20.0,6);

UPDATE marques_cascade SET id=20 WHERE id=6;

-- ON UPDATE SET NULL
ALTER TABLE articles_cascade DROP CONSTRAINT FK_articles_marques_cascade;

ALTER TABLE articles_cascade ADD CONSTRAINT FK_articles_marques_cascade
FOREIGN KEY(marque)
REFERENCES marques_cascade(id) ON UPDATE SET NULL;

UPDATE marques_cascade SET id=30 WHERE id=20;