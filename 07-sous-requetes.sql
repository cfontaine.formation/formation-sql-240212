USE bibliotheque;

-- Requête imbriqué retourne un seul résultat
SELECT titre,annee FROM livres
WHERE annee=(
	SELECT round(avg(annee),0) 
	FROM livres 
	WHERE livres.genre=1
	); 
	
-- IN
-- La requête externe peut utiliser la commande IN pour filtrer
-- les lignes qui possèdent une des valeurs retournées par la requête interne
SELECT titre,annee FROM livres 
WHERE annee IN(
	SELECT annee FROM livres
	INNER JOIN genres ON genres.id=livres.genre
	WHERE genres.nom='horreur'
	);

-- ANY ou SOME
-- ANY permet de comparer une valeur avec le résultat d’une sous-requête
-- On peut vérifier si UNE valeur est =, !=, >,<,>=,>=
SELECT titre,annee FROM livres 
WHERE annee < ANY(
 	SELECT annee FROM livres
	INNER JOIN genres ON genres.id=livres.genre
	WHERE genres.nom='science-fiction'
	);
	
-- ALL
-- ALL permet de comparer une valeur dans l’ensemble de valeurs d’une sous-requête
-- On peut vérifier qu’une condition est =, !=, >,<,>=,>= pour TOUS les résultats retournés par une sous-requête
SELECT titre,annee FROM livres 
WHERE annee <> ALL(
 	SELECT annee FROM livres
	INNER JOIN genres ON genres.id=livres.genre
	WHERE genres.nom='science-fiction'
	);
	
-- EXISTS
-- on teste la  présence ou non de lignes lors de l’utilisation d’une sous-requête
-- sous-requete corrélé ->  elle fait référence à des colonnes de la table externe
SELECT genres.nom FROM genres 
WHERE EXISTS(
	SELECT id 
	FROM livres 
	WHERE livres.genre =genres.id -- référence à la colonne id de la table externe genres
	);
	
SELECT DISTINCT auteurs.prenom, auteurs.nom, genres.nom FROM auteurs
INNER JOIN livre2auteur ON auteurs.id =livre2auteur.id_auteur 
INNER JOIN livres ON livres.id=livre2auteur.id_livre
INNER JOIN genres ON livres.genre= genres.id
WHERE EXISTS(
	SELECT id 
	FROM livres 
	WHERE livres.genre =genres.id
	);

-- On peut utiliser une sous-requête comme colonne dans le résultat
SELECT titre,(SELECT nom FROM genres WHERE genres.id=livres.genre) FROM livres

-- Exercice sous-requête
USE world;
-- Tous les pays avec une capitale de plus d'un million d'habitant
SELECT name FROM country WHERE capital IN 
	(	SELECT city.id FROM city
		WHERE population > 1000000);

-- Liste des pays ayant une langue parlée par moins de 10% de la population
SELECT name
FROM country
WHERE code IN 
	(SELECT DISTINCT countrylanguage.CountryCode 
	 FROM countrylanguage
	 WHERE countrylanguage.Percentage  < 10); 
	
	