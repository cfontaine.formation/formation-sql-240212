USE bibliotheque;

EXPLAIN SELECT a.prenom, a.nom, titre, genres.nom genre,annee FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs a ON a.id=livre2auteur.id_auteur
INNER JOIN genres ON genres.id= livres.genre
WHERE a.nom LIKE 'S_%' AND annee <1970
ORDER BY annee DESC,genre; 