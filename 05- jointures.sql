USE bibliotheque;

-- Jointure Interne 

-- Avec WHERE
-- Afficher le titre, l'annee et le genre du livre
SELECT titre, genres.nom,livres.genre,genres.id FROM livres, genres
WHERE livres.genre=genres.id ;

-- INNER JOIN

-- Relation 1,N
-- On va joindre la table livres et la table genres en utilisant l'égalité entre
-- La clé primaire de genres -> id et la clé étrangère de livre -> genre
SELECT titre,annee,genres.nom AS genre FROM livres
INNER JOIN genres ON livres.genre=genres.id;

-- Afficher le prénom et le nom de l'auteur et le nom du pays
SELECT prenom, auteurs.nom,pays.nom FROM auteurs
INNER JOIN pays ON auteurs.nation = pays.id;

-- Afficher le titre, l'année de sortie et le genre du livre qui sont sortie entre 1980 et 1990
-- pour les genres policier et cyberpunk
SELECT titre,annee,genres.nom AS genre FROM livres
INNER JOIN genres ON livres.genre=genres.id
WHERE genres.nom IN ('policier','cyberpunk')
AND annee BETWEEN 1980 AND 1990;

-- Relation n-n
-- On va joindre : la table livres et la table de jointure livre2auteur
-- On va joindre le resultat de la jointure précédente avec la table auteurs
-- Afficher le titre du livre, année de sortie du livre, le prénom et le nom de son auteurs
SELECT prenom, nom, titre, annee FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur;

SELECT prenom, auteurs.nom, titre, genres.nom,annee FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur
INNER JOIN genres ON genres.id= livres.genre;

SELECT a.prenom, a.nom, titre, genres.nom genre,annee FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs a ON a.id=livre2auteur.id_auteur
INNER JOIN genres ON genres.id= livres.genre
WHERE a.nom LIKE 'S_%' AND annee <1970
ORDER BY annee DESC,genre;

-- Exercice Jointure interne
USE world;
-- Afficher chaque nom de pays et le nom de sa capitale
SELECT country.name,city.name FROM city
INNER JOIN country ON city.id=country.Capital;

-- Afficher les nom de pays et le nom de ses villes classer par nom de pays puis par nom de ville
-- en ordre alphabétique (ne pas afficher les pays sans ville)
SELECT country.name AS pays, city.name AS villes FROM country
INNER JOIN city ON city.CountryCode = country.Code
WHERE city.name IS NOT NULL 
ORDER BY country.name, city.name;

-- Afficher les noms de  pays, la  langue et le 
-- pourcentage  classé par pays et par  pourcentage décroissant
SELECT country.name,countrylanguage.language ,countrylanguage.percentage FROM country
INNER JOIN countrylanguage ON country.code=countrylanguage.countrycode 
ORDER BY country.name, countrylanguage.percentage DESC;

USE exemple;

-- Produit cartésien -> CROSS JOIN
-- On obtient toutes les combinaisons possible entre les 2 tables
-- Le nombre d'enregistrements obtenu est égal au nombre de lignes de la première table multiplié par le nombre de lignes de la deuxième table.
CREATE TABLE plats
( 
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

CREATE TABLE boissons
( 
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO plats(nom) VALUES
('Céréale'),
('Pain'),
('Oeuf sur le plat');

INSERT boissons(nom) VALUES 
('Café'),
('Thé'),
('Jus d''orange');

-- On obtient toutes les combinaisons possible entre les boissons et les plats
SELECT plats.nom AS plat, boissons.nom AS boisson FROM plats 
CROSS JOIN boissons;

-- Jointure externe
USE bibliotheque ;

-- Jointure externe: LEFT JOIN ou RIGTH JOIN
SELECT nom,titre,annee FROM genres
LEFT JOIN livres ON genres.id= livres.genre;
-- équivalent avec un right join
SELECT nom,titre,annee FROM livres
RIGHT JOIN genres ON genres.id= livres.genre;

SELECT titre,annee,nom FROM livres
LEFT JOIN genres ON genres.id= livres.genre;
-- équivalent avec un right join
SELECT nom,titre,annee FROM genres
RIGHT JOIN livres ON genres.id= livres.genre;

-- Obtenir tous les genres qui ne sont pas représentés dans la bibliotheque
SELECT nom,titre,annee FROM genres
LEFT JOIN livres ON genres.id= livres.genre
WHERE livres.id IS NULL;

-- Obtenir tous les auteurs dont la nationalité n'a pas été renseigné
SELECT a.prenom,a.nom,p.nom AS nationalite  FROM auteurs a
LEFT JOIN pays AS p ON a.nation  = p.id
WHERE p.id IS NULL;

-- Obtenir tous les pays qui n'ont d'auteur
SELECT a.prenom,a.nom,p.nom AS nationalite  FROM auteurs a
RIGHT JOIN pays AS p ON a.nation = p.id  
WHERE a.id IS NULL;

-- FULL JOIN
-- SELECT a.prenom,a.nom,p.nom AS nationalite  FROM auteurs a
-- FULL JOIN pays AS p ON a.nation = p.id ;

-- FULL JOIN n'est pas encore supporté par MySql/MariaDb
-- Mais on peut le réaliser avec cette requète.
SELECT a.prenom,a.nom,p.nom AS nationalite  FROM auteurs a
LEFT JOIN pays AS p ON a.nation  = p.id 
UNION
SELECT a.prenom,a.nom,p.nom AS nationalite  FROM auteurs a
RIGHT JOIN pays AS p ON a.nation = p.id;

-- Jointure naturelle -> uniquement mysql et mariadb
-- la clé primaire et la clé étrangère doivent avoir le même nom

-- Changer le nom de la clé primaire  id -> genre
ALTER TABLE livres DROP CONSTRAINT fk_genres; 
ALTER TABLE genres CHANGE id genre INT; 

ALTER TABLE livres ADD CONSTRAINT fk_genres
FOREIGN KEY (genre)
REFERENCES genres(genre);

-- Jointure naturelle
SELECT titre, annee,nom FROM livres
NATURAL JOIN genres;

-- Changer le nom de la clé primaire genre -> id
ALTER TABLE livres DROP CONSTRAINT fk_genres; 
ALTER TABLE genres CHANGE genre id INT; 

ALTER TABLE livres ADD CONSTRAINT fk_genres
FOREIGN KEY (genre)
REFERENCES genres(id);


-- Exercice: Jointure interne et externe
USE world;

-- Afficher: les noms de  pays, la  langue et le  pourcentage  classé par pays et par  pourcentage décroissant. mais on ne veut obtenir que les langues officielles
SELECT country.name, countrylanguage.LANGUAGE,countrylanguage.percentage FROM country
INNER JOIN countrylanguage ON country.code= countrylanguage.CountryCode
WHERE countrylanguage.IsOfficial ='T'
ORDER BY country.name,countrylanguage.percentage  DESC;

-- Afficher le nom des pays sans ville
SELECT country.name FROM country
LEFT JOIN city ON city.CountryCode = country.Code 
WHERE city.name IS NULL;

-- Afficher tous les pays qui parlent français
SELECT country.name FROM country 
INNER JOIN countrylanguage ON country.code= countrylanguage.CountryCode 
WHERE countrylanguage.LANGUAGE='French'; 

USE elevage;
-- Afficher la liste des races de chiens qui sont des chiens de berger
SELECT race.nom FROM race
INNER JOIN espece ON race.espece_id = espece .id 
WHERE espece.nom_courant='Chien' AND race.nom LIKE '%berger%';

-- Afficher la liste des chats et des perroquets amazones, avec  leur espèce (nom latin) et leur race s'ils en ont une
-- Regroupez les chats ensemble, les perroquets ensemble et, au sein de l'espèce, regroupez les races.
SELECT animal.nom, espece.nom_courant , race.nom FROM espece
INNER JOIN animal ON animal.espece_id =espece.id 
LEFT JOIN race ON animal.race_id =race.id
WHERE espece.nom_courant IN ('Chat','Perroquet amazone')
ORDER BY espece.nom_courant ,race.nom;

-- Auto jointure
-- Une table que l'on joindre avec elle même

-- Une auto joiture est souvent utilisé pour représenter
-- une hierarchie en SQL (relation parent->enfant)
USE exemple;

CREATE TABLE salaries
(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50) NOT NULL,
	nom VARCHAR(50) NOT NULL,
	manager INT,
	
	CONSTRAINT fk_manager
	FOREIGN KEY (manager)
	REFERENCES salaries(id)
);

INSERT INTO salaries(prenom, nom,manager) VALUES 
('John','Doe',NULL),
('Jane','Doe',1),
('Alan','Smithee',2),
('Jo','Dalton',1),
('yves','roulot',4);

SELECT employes.prenom AS emp_prenom,employes.nom AS emp_nom
 	   ,managers.prenom AS mgmt_prenom,managers.nom AS mgmt_nom
FROM salaries AS employes
LEFT JOIN salaries AS managers ON employes.manager= managers.id;

-- Exercice Auto-jointure
USE elevage;
-- Afficher  la liste des enfants de Bouli
 le nom de ces parents
SELECT Animal.nom AS enfant,Pere.nom AS pere FROM animal 
INNER JOIN animal AS pere ON pere.id = animal.pere_id
WHERE pere.nom='Bouli';

-- Afficher la liste des chats dont on connaît les parents, ainsi que
SELECT animal.nom,pere.nom AS pere, mere.nom AS mere FROM animal
INNER JOIN animal AS pere ON pere.id=animal.pere_id
INNER JOIN animal AS mere ON mere.id=animal.mere_id 
INNER JOIN espece ON animal.espece_id =espece.id
WHERE espece.nom_courant = 'chat';
