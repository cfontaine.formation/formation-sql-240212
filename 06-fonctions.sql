USE bibliotheque;

-- Fonction arithmétiques

-- Syntaxe d'une fonction
-- nom_fonction(param1,param2 ...)

-- Fonctions arithmétiques
-- CEIL(1.4) -> 2, FLOOR(1.6) -> 1, ROUND(1.4628476,3) -> 1.463, TRUNCATE(1.4628476,3) -> 1,462
-- RAND() -> nombre aléatoire entre 0 et 1
SELECT CEIL(1.4) AS CEIL, FLOOR(1.6) AS FLOOR,
		TRUNCATE(1.4628476,3) AS TRUNCATE, ROUND(1.4628476,3) AS ROUND,
		RAND();
	
-- On peut utiliser rand() pour sélectionner aléatoirement 5 livres 
SELECT titre FROM livres ORDER BY RAND() LIMIT 5;

-- Fonction Chaine de caractère
-- - Taille de la chaine de caractère nom en octets
-- - Nombre de caractère de la chaine de caractère nom 
SELECT nom, LENGTH(nom),char_length(nom) FROM auteurs; 

-- Nombre de caractère de la chaine de caractère titre, trié nombre de caractère décroissent
SELECT titre,char_length(titre) AS nb_caractere FROM livres ORDER BY  nb_caractere DESC;

-- On ne sélectionne que les lignes de la table livres dont le nom fait moins de 7 caratères
SELECT titre,char_length(titre) FROM livres WHERE char_length(titre) <7;

-- concat -> Concaténation de chaînes
SELECT CONCAT(prenom,' ',nom,' ','(',naissance,')') FROM auteurs;

-- concat_ws -> Concaténation de chaînes avec un séparateur (->1er paramètre)
SELECT CONCAT_ws(' ',prenom,nom,naissance) FROM auteurs;

-- space(20) -> retourne une chaîne contenant 20 espaces
SELECT CONCAT('|',SPACE(20),'|');

-- insert: Insertion d'une chaîne à une position pos et pour num caractères
-- Insertion d'une chaîne ---- au 3 caractères et pour remplacer 2 caractères
-- Ja---mes -> Ja---s
SELECT prenom, insert(prenom,3,0,'---') FROM auteurs;

-- remplace: Remplace toutes les occurrences d'une sous-chaîne par une nouvelle chaîne
-- remplace dans le nom er par ---
-- Hebert -> H---b---t
SELECT nom, REPLACE(nom,'er','---') FROM auteurs;

-- repeat -> répéte le nom 3 fois
-- reverse -> Inverse les caractères du nom (Ellroy -> yorllE)
SELECT nom,repeat(nom,3),reverse(nom) FROM auteurs;

-- left(prenom,2) -> Extrait 2 caractère en partant de la gauche du prénom
-- right(prenom,2) -> Extrait 2 caractère en partant de la droite du prénom
-- substr(prenom,2,3) -> Extraction d'une sous chaîne du prénom à partir du 2ème caractères et pour 3 caractères
SELECT prenom, LEFT(prenom,2),RIGHT(prenom,2),substr(prenom,2,3) FROM auteurs; 

-- Renvoie la position de la première occurrence de la chaîne 'am', dans le prenom
SELECT prenom, position('am' IN prenom ) FROM auteurs;

-- FIND_IN_SET Renvoie la position de la chaîne aze dans la chaîne 'ert,qez,sdvqh,aze,rty'
-- contenant une liste de sous-chaîne séparé par une virgule -> 4
SELECT FIND_IN_SET('aze','ert,qez,sdvqh,aze,rty');

-- FIELD Renvoie la position de 'ert' dans la liste de valeur 'gjhghj','ggujh','ert','aze','rty'
SELECT FIELD('ert','gjhghj','ggujh','ert','aze','rty'); -- -> 3

-- LTRIM -> Retire les caractère blanc à gauche de la chaîne
-- RTRIM -> Retire les caractère blanc à droite de la chaîne
-- TRIM ->Retire les caractère blanc à droite et à gauche
SELECT LTRIM('       helloworld        '),
	   RTRIM('       helloworld        '),
	    TRIM('       helloworld        ');

-- lower(prenom) -> Conversion en minuscule du prénom
-- upper(prenom) -> Conversion en majuscule du prénom
SELECT LOWER(prenom),UPPER(nom) FROM auteurs;

-- strcmp -> Compare 2 chaînes : 
-- 'bonjour' < 'hello' → -1
-- 'bonjour' > 'hello' → 1
-- 'bonjour' = 'bonjour' → 0 
SELECT STRCMP('bonjour','hello'),STRCMP('hello','bonjour'),STRCMP('bonjour','bonjour');

-- 1980 -> 4 chiffres après la virgule 1,980.0000
SELECT FORMAT(annee,4) FROM livres;

-- Fonction Temporelle
-- CURRENT_DATE() -> Date courante
-- CURRENT_TIME() -> Heure courante
-- CURRENT_TIMESTAMP() ou NOW() -> Date et heure courante
SELECT current_date(),current_time(),current_timestamp(),now();

-- DATE -> Extrait la date de la Date et de l'heure courante -> 2023-02-14
-- Day -> Extrait le jour de la Date et de l'heure courante -> 14
SELECT DATE(now()),DAY(NOW()),LAST_DAY(now());

-- MONTH -> Extrait le mois -> 10
-- YEAR -> Extrait l'année -> 2023
-- dayofmonth -> Extrait le jour -> 20
-- quarter -> retourne le trimestre d'une date -> 3
-- WEEK -> Numéro de semaine d'une date (0 à 53)
SELECT nom,naissance,MONTH(naissance),YEAR(naissance),dayofmonth(naissance),quarter(naissance),week(naissance),dayofyear(naissance) FROM auteurs;

-- Requete pour obtenir l'age d'un livre 
SELECT titre, YEAR(now())-annee  AS age FROM livres;


-- dayname -> Extrait le nom du jour de la semaine  -> Wednesday
-- monthname -> Extrait le nom du mois -> October
-- last_day -> Le dernier jour du mois (30,31,29,28)
-- DAYOFYEAR -> Le jour de l'année (1 à 366)
SELECT nom,naissance,dayname(naissance),monthname(naissance),Last_Day(naissance),DAYOFYEAR(naissance)FROM auteurs; 

-- dayofweek -> Le numéro du jour de la semaine d'une date (1→ dimanche … 7→ samedi)
-- weekday -> Numéro de jour de la semaine (0 → lundi … 6 → dimanche)
SELECT dayofweek(naissance),weekday(naissance) FROM auteurs;

-- Nombre de jour entre 2 dates
SELECT DATEDIFF('2024-12-31',current_date()); -- 321

-- Age des auteurs lorqu'ils sont décédés
SELECT prenom, nom,ROUND(DATEDIFF(deces,naissance)/365) FROM auteurs WHERE deces IS NOT NULL;

-- ADDDATE -> Ajoute un intervalle à une date
-- SUBDATE -> Soustrait un intervalle à une date
SELECT DATE_ADD(current_date(), INTERVAL 2 DAY),DATE_SUB(current_date(), INTERVAL 1 YEAR);

-- DATE_FORMAT -> Formater une date
-- https://mariadb.com/kb/en/date_format/
SELECT date_format(naissance,'%m %Y (%j) %a') FROM auteurs;

-- STR_TO_DATE -> Convertir une chaîne en date suivant un format
SELECT str_to_date('10/09/2018','%d/%m/%Y');

-- L'heure
-- HOUR -> extraire les heures
-- MINUTE -> extraire les minutes
-- SECOND -> extraire les secondes
-- microsecond -> extraire les microsecondes
SELECT HOUR(current_time()),MINUTE(current_time()),SECOND(current_time()),microsecond(current_time()) ;

-- timediff -> Différence entre 2 heures
-- subtime -> Soustrait un intervalle à une heure
-- addtime -> Ajoute un intervalle à une heure
SELECT timediff('12:30:00',current_time()),subtime(current_time(),'05:00:00'),addtime(current_time(),'05:00:00');

-- time_to_sec -> Conversion d’une heure en seconde
SELECT time_to_sec(current_time());

-- TIME_FORMAT -> Formater une heure suivant un format
SELECT TIME_FORMAT(current_time(),'%l:%i:%s %p');

-- Fonction d'agrégation
SELECT count(id),min(annee),max(annee), truncate(avg(annee),0) FROM livres; 

-- Autre fonction
-- COALESCE -> Retourne la première valeur non-null de la liste
SELECT prenom, nom, COALESCE(deces,'vivant') FROM auteurs;

-- NULLIF -> Retourne NULL si les 2 paramètres sont égaux sinon retourne le premier paramètre
SELECT annee,NULLIF(annee,1954) FROM livres;

-- current_user -> utilisateur courant
-- DATABASE () -> base de donnée courante
-- VERSION() -> version de la sgbd
-- last_insert_id() -> L’id AUTO_INCREMENT de la dernière ligne qui a été insérée ou modifiée
SELECT current_user(),DATABASE(),VERSION(),last_insert_id();

-- Fonction if
-- On affiche pour les auteurs vivants leur age et pour les auteurs morts leur age au moment de leur mort
SELECT prenom,nom,IF(deces IS NULL,ROUND(DATEDIFF(now(),naissance)/365),
      ROUND(DATEDIFF(deces,naissance)/365)) FROM auteurs;

-- Fonction CASE
-- CASE utilisant des conditions
SELECT annee, titre,
CASE
	WHEN annee>2000 THEN 'Moderne'
	WHEN annee BETWEEN 1901 AND 2000 THEN '20 eme siecle'
	WHEN annee BETWEEN 1801 AND 1900 THEN '19 eme siecle'
	ELSE 'Livre ancien'
END 
FROM livres;
END

-- CASE utilisant des valeurs
SELECT titre, genre,
CASE(genre)
	WHEN 1 THEN 'Roman policier'
	WHEN 6 THEN 'Roman science-fiction'
	WHEN 7 THEN 'Drame'
	ELSE 'Une autre catégorie'
END
FROM livres;

-- Exercice Fonctions :
USE world;
-- Nombre de pays présents dans la table Country
SELECT count(code) FROM country;

-- Écrire une requête pour générer un code qui a pour forme les 3 première lettres de la ville concaténé avec la chaine '0000'et le nombre de caractère de la ville et séparé par -
SELECT CONCAT_WS('-',LEFT(name,3),'0000',char_length(name)) AS code FROM city; 

-- Afficher le nombre de pays en europe
SELECT count(code) FROM country 
WHERE continent='Europe'; 

USE elevage;
-- Afficher le nombre de race dans la table Race
SELECT count(id) FROM race;

-- Afficher la liste des animaux nés en 2006
SELECT nom FROM animal WHERE YEAR(date_naissance)=2006;

-- Afficher le nom de tous les animaux nés dans les huit premières semaines d'une année.
SELECT nom ,date_naissance  FROM animal WHERE WEEK(date_naissance)<=8;

-- Afficher les chats dont la deuxième lettre du nom est un a
SELECT nom FROM animal 
INNER JOIN espece ON animal.espece_id = espece.id 
WHERE espece.nom_courant='chat' AND POSITION('a' IN nom)=2;
-- ou
SELECT nom FROM animal 
INNER JOIN espece ON animal.espece_id = espece.id 
WHERE espece.nom_courant='chat' AND nom LIKE '_a%';

-- Afficher les chiens dont le nom a un nombre pair de lettres
SELECT nom FROM animal 
INNER JOIN espece ON animal.espece_id = espece.id 
WHERE espece.nom_courant='chien' AND char_length(nom)%2=0 

-- Afficher le nombre de chiens dont on connait le père
SELECT count(animal.id) FROM animal 
INNER JOIN espece ON animal.espece_id = espece.id 
WHERE espece.nom_courant='chien' AND animal.pere_id IS NOT NULL;
