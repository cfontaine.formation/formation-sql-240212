USE bibliotheque;

-- Afficher la liste de procédure stockée
SHOW PROCEDURE STATUS;

-- Appel de la procedure stockée
CALL auteur_vivant();

-- Variable utilisateur / session -> variable globale @nom_variable

-- Afficher le contenu d'une variable 
SET @var_global='test var globale';

-- Afficher le contenu d'une variable 
SELECT @var_global ;

CALL test_variable();
SELECT @var_global ;
-- On ne peut accèder à une variable locale en dehors la procédure où elle est déclarée
-- SELECT var_local;

-- Exemple passage de paramètre
-- 1 et 2 ->  paramètres en entrée
-- le résultat de l'addition va être placé dans la variable globale @var_global
CALL addition(1,2,@var_global);
SELECT @var_global ;

CALL nb_livre_genre('drame',@var_global);
SELECT @var_global ;

USE world;
-- Exercice: procédure stockée
CALL nb_ville_pays('ITA',@var_global);
SELECT @var_global ;

USE bibliotheque;
-- Exemple de conditon IF
CALL parite(4,@var_global);
SELECT @var_global;

CALL nb_livre_genre2('drame',@var_global);
SELECT @var_global ;

CALL nb_livre_genre2('frjbgjek',@var_global);
SELECT @var_global ;

-- Exemple de condition CASE
CALL categorie_age_livre (1980,@var_global);
SELECT @var_global ;

-- Exemple boucle LOOP
CALL somme_multi_loop(5,@var_global);
SELECT @var_global;

-- Exemple boucle WHILE
CALL somme_multi_while(5,@var_global);
SELECT @var_global;

-- Exemple boucle REPEAT
CALL somme_multi_repeat(5,@var_global);
SELECT @var_global;

-- Exemple curseur
CALL auteurs_by_livre(133,@var_global) ;
SELECT @var_global ;

-- Supression de toutes les procedures stockées
DROP PROCEDURE auteur_vivant;
DROP PROCEDURE test_variable;
DROP PROCEDURE addition;
DROP PROCEDURE nb_livre_genre;
DROP PROCEDURE parite;
DROP PROCEDURE nb_livre_genre2;
DROP PROCEDURE categorie_age_livre;
DROP PROCEDURE somme_multi_loop;
DROP PROCEDURE somme_multi_while;
DROP PROCEDURE somme_multi_repeat;
DROP PROCEDURE auteurs_by_livre;

USE world;
DROP PROCEDURE nb_ville_pays