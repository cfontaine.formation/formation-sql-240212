-- Base de données: Bibliotheque 
-- (MySQL/MariaDB)
-- Novembre 2023
-- -----------------------------

SET FOREIGN_KEY_CHECKS = 0;

DROP DATABASE IF EXISTS bibliotheque;
CREATE DATABASE IF NOT EXISTS bibliotheque;

USE bibliotheque;

-- Table pays
CREATE TABLE `pays` (
  id int PRIMARY KEY  AUTO_INCREMENT,
  nom varchar(50) NOT NULL
);

INSERT INTO `pays` VALUES (1,'États-Unis'),(2,'Suède'),(3,'Grande-Bretagne'),(4,'France'),(5,'Espagne'),(6,'Italie'),(7,'Russie'),(8,'Tchécoslovaquie'),(9,'Belgique'),
(10,'Portugal'),(11,'Bolivie'),(12,'Chine');

-- Table genres
CREATE TABLE `genres` (
  id int PRIMARY KEY AUTO_INCREMENT,
  nom varchar(30) NOT NULL
);

INSERT INTO genres VALUES 
(1,'Policier'),(2,'Autobiographie'),(3,'Horreur'),(4,'Fantastique'),(5,'Thriller'),(6,'Science-fiction'),(7,'Drame'),(8,'Espionnage'),
(9,'Historique'),(10,'Politique'),(11,'Fantasy'),(12,'Naturaliste'),(13,'Anticipation'),(14,'Aventure'),(15,'Cyberpunk'),(16,'Humour'),(17,'Steampunk'),(18,'Fable');

-- Table livres
CREATE TABLE livres (
  id int PRIMARY KEY AUTO_INCREMENT,
  titre varchar(240) NOT NULL,
  annee int NOT NULL,
  genre int,
  CONSTRAINT fk_genres
  FOREIGN KEY (genre) REFERENCES genres (id)
);

INSERT INTO livres VALUES
(1,'Lune sanglante',1982,1),(2,'Le Dahlia noir',1987,1),(3,'L.A. Confidential',1990,1),(4,'American Tabloïd',1995,1),(5,'American Death Trip',2001,1), (6,'Underworld USA ',2009,1),
(7,'Ma part d\'ombre',1997,2),(8,'Carrie',1974,3), (9,'Shining,l\'enfant lumière	',1977,3),
(10,'Le Fléau',1978,3),(11,'Dead Zone',1979,4),(12,'Christine',1983,3),(13,'Ça',1988,3),(14,'Misery',1987,5),(15,'Dune',1965,6),(16,'Le Messie de Dune',1969,6),(17,'Les Enfants de Dune',1978,6),(18,'L\'Empereur-Dieu de Dune',1981,6),(19,'Les Hérétiques de Dune',1985,6),(20,'La Maison des mères',1986,6),(21,'Les androïdes rêvent-ils de moutons électriques ?	',1968,6),(22,'Ubik',1969,6),(23,'Substance Mort',1978,6),(24,'Le Maître du Haut Château	',1962,6),(25,'Les Marteaux de Vulcain',1975,4),(26,'Les Hommes qui n\'aimaient pas les femmes	',2005,1),(27,'La Fille qui rêvait d\'un bidon d`\'essence et d\'une allumette',2005,1),(28,'La Reine dans le palais des courants d\'air',2005,1),(29,'Casino Royale',1953,8),(30,'Live and Let Die',1954,8),(31,'Moonraker	',1955,8),(32,'Diamonds Are Forever',1956,8),(33,'From Russia With Love	',1957,8),(34,'Dr. No',1958,8),(35,'Goldfinger',1959,8),(36,'For Your Eyes Only',1960,8),(37,'Thunderball',1961,8),(38,'The Spy Who Loved Me',1962,8),(39,'On Her Majesty\'s Secret Service',1963,8),(40,'You Only Live Twice',1964,8),(41,'The Man With The Golden Gun',1965,8),(42,'Octopussy and the Living Daylights',1966,8),(43,'Don Quichotte	',1605,14),(44,'Le Désert des Tartares',1940,7),(45,'Le K',1966,4),(46,'Le Nom de la rose',1980,7),(47,'Les Cosaques',1863,9),(48,'Guerre et Paix',1865,9),(49,'Anna Karénine',1877,7),(50,'Résurrection',1899,10),(51,'Harry Potter à l\'école des sorciers',1997,11),(52,'Harry Potter et la Chambre des secrets',1998,11),(53,'Harry Potter et le Prisonnier d\'Azkaban',1999,11),(54,'Harry Potter et la Coupe de feu',1999,11),(55,'Harry Potter et l\'Ordre du phénix',2003,11),(56,'Harry Potter et le Prince de sang-mêlé',2005,11),(57,'Harry Potter et les Reliques de la Mort',2007,11),(58,'Le Fermier Gilles de Ham	',1949,11),(59,'Le Hobbit',1937,11),(60,'La Communauté de l\'Anneau	',1954,11),(61,'Les Deux Tours',1954,11),(62,'Le Retour du roi',1955,11),(63,'La Machine à explorer le temps',1885,6),(64,'L\'Île du docteur Moreau',1896,6),(65,'L\'Homme invisible',1897,6),(66,'La Guerre des mondes',1898,6),(67,'La Planète des singes',1963,6),(68,'L\'Appel de la forêt',1903,14),(69,'Croc-Blanc',1923,14),(70,'Le Bureau des assassinats',1963,1),(71,'Le Cabaret de la dernière chance',1913,2),(72,'L\'Appel de Cthulhu',1928,4),(73,'L\'Affaire Charles Dexter Ward',1941,4),(74,'Le Cauchemar d\'Innsmouth',1931,3),(75,'Celui qui hantait les ténèbres',1936,4),(76,'La Peur qui rôde',1923,4),(77,'Le Modèle de Pickman',1926,4),(78,'La Fortune des Rougon',1871,12),(79,'La Curée',1872,12),(80,'Le Ventre de Paris',1873,12),(81,'La Conquête de Plassans',1874,12),(82,'La Faute de l\'abbé Mouret',1875,12),(83,'Son Excellence Eugène Rougon',1876,12),(84,'L\'Assommoir',1878,12),(85,'Une page d\'amour',1880,12),(86,'Nana',1882,12),(87,'Pot-Bouille',1883,12),(88,'Au Bonheur des Dames',1883,12),(89,'La Joie de vivre',1885,12),(90,'Germinal',1886,12),(91,'L\'Œuvre',1887,12),(92,'La Terre',1888,12),(93,'Le Rêve',1890,12),(94,'L\'Argent	',1891,12),(95,'La Débâcle',1892,12),(96,'Le Docteur Pascal',1893,12),(97,'Neuromancien',1984,15),(98,'Comte Zéro',1986,15),(99,'Mona Lisa s\'éclate',1988,15),(100,'L\'Atlantide',1919,14),(101,'Chroniques martiennes',1950,6),(102,'Fahrenheit 451',1955,6),(103,'Le Meilleur des mondes',1932,13),(104,'L\'Affaire N\'Gustro',1972,1),(105,'Nada',1972,1),(106,'Morgue pleine',1972,1),(107,'Que d\'os !',1976,1),(108,'Le Petit Bleu de la côte ouest',1976,1),(109,'La Position du tireur couché',1981,1),(110,'Les aventures de Huckleberry Finn',1884,14),(111,'Les aventures de Tom Sawyer',1876,14),(112,'Le Guide du voyageur galactique',1982,6),(113,'Le Dernier Restaurant avant la fin du monde',1982,6),(114,'La Vie, l\'Univers et le Reste',1983,6),(115,'Salut, et encore merci pour le poisson',1994,6),(116,'Globalement inoffensive',1994,6),(117,'Le Procès',1925,7),(118,'1984',1949,6),(119,'La Marie du port',1938,1),(120,'Les Fantômes du chapelier',1949,1),(121,'L\'Aîné des Ferchaux',1945,1),(122,'La Veuve Couderc',1942,1),(123,'L\'Homme qui regardait passer les trains',1938,1),(124,'Le Relais d\'Alsace',1931,1),(125,'La Maison du canal',1933,1),(126,'Le Chien jaune',1931,1),(127,'Au rendez-vous des Terre-Neuvas',1931,1),(128,'L\'Affaire Saint-Fiacre',1932,1),(129,'L\'Écluse numéro 1',1933,1),(130,'L\'Ombre chinoise',1931,1),(131,'Un crime en Hollande',1931,1),(132,'Celle qui n\'était plus',1952,1),(133,'Les Visages de l\'ombre',1953,1),(134,'L\'ingénieur aimait trop les chiffres',1959,1),(135,'D\'entre les morts',1954,1),(136,'Fantômas',1911,1),(137,'Juve contre Fantômas',1911,1),(138,'Le Mort qui tue',1911,1),(139,'L\'Assassin de Lady Beltham',1912,1),(140,'La Cravate de chanvre',1913,1),(141,'Laissez bronzer les cadavres !',1971,1),(142,'Le Samouraï virtuel',1992,15);

-- Table auteurs
CREATE TABLE auteurs (
  id int PRIMARY KEY AUTO_INCREMENT,
  prenom varchar(50) NOT NULL,
  nom  varchar(50) NOT NULL,
  naissance date NOT NULL,
  deces date DEFAULT NULL,
  nation int DEFAULT NULL,
  CONSTRAINT fk_nation FOREIGN KEY (nation) REFERENCES pays (id)
);

INSERT INTO auteurs VALUES (1,'James','Ellroy','1948-03-04',NULL,1),(2,'Stephen','King','1947-09-21',NULL,1),(3,'Frank','Herbert','1920-10-08','1986-02-11',1),
(4,'Philip K.','Dick','1928-12-16','1982-03-02',1),(5,'Stieg','Larsson','1954-08-15','2004-11-09',2),(6,'Ian','Fleming','1908-05-28','1964-08-12',3),
(7,'Pierre Louis','Boileau','1906-04-28','1989-01-16',4),(8,'Thomas','Narcejac','1908-07-03','1998-06-07',4),(9,'Miguel','de Cervantes','1547-09-29','1616-04-23',5),(10,'Dino','Buzzati','1906-10-16','1972-01-28',6),(11,'Umberto','Eco','1932-01-05','2016-02-19',6),(12,'Léon','Tolstoï','1828-09-09','1910-11-20',7),(13,'Pierre','Souvestre','1874-06-01','1914-02-25',4),(14,'Marcel','Allain','1885-09-15','1969-08-25',4),(15,'J. K.','Rowling','1965-07-31',NULL,3),(16,'J. R. R.','Tolkien','1892-01-03','1973-09-02',3),(17,'H. G.','Wells','1866-09-21','1946-08-13',3),(18,'Pierre','Boulle','1912-02-20','1994-01-31',4),(19,'Jack','London','1876-01-12','1916-11-22',1),(20,'H P','Lovecraft','1890-08-20','1937-03-15',1),(21,'Émile','Zola','1840-04-02','1902-09-29',4),(22,'William','Gibson','1948-03-17',NULL,1),(23,'Pierre','Benoit','1886-07-16','1962-03-03',4),(24,'Ray','Bradbury','1920-08-22','2012-06-05',1),(25,'Aldous','Huxley','1894-07-26','1963-11-22',3),(26,'Jean-Patrick','Manchette','1942-12-19','1995-06-03',4),(27,'Jean-Pierre','Bastid','1937-02-04',NULL,4),(28,'Mark','Twain','1835-11-30','1910-04-21',1),(29,'Douglas','Adams','1952-03-11','2001-05-11',3),(30,'Franz','Kafka	','1883-07-03','1924-06-03',8),(31,'George','Orwell','1903-06-25','1950-01-21',3),(32,'Georges','Simenon','1903-02-13','1989-09-04',9),(33,'Victor','Hugo','1802-02-26','1885-05-22',4),(34,'Edgar Allan','Poe','1809-01-19','1849-10-07',1),(35,'John ','Le Carré','1931-10-19','2020-12-12',3),(36,'Neal','Stephenson','1959-10-31',NULL,1),
(37, 'Honoré', 'de Balzac', '1799-05-20', '1850-08-18', NULL), (38, 'Guy', 'de Maupassant', '1850-08-05', '1893-07-06', NULL), (39, 'Roberto', 'Saviano', '1979-09-22', NULL, NULL);

-- Table livre2auteur
CREATE TABLE livre2auteur (
  id_livre int NOT NULL,
  id_auteur int NOT NULL,
  PRIMARY KEY (id_livre,id_auteur),
  CONSTRAINT fk_auteurs FOREIGN KEY (id_auteur) REFERENCES auteurs (id),
  CONSTRAINT fk_livres FOREIGN KEY (id_livre) REFERENCES livres (id)
);

INSERT INTO livre2auteur VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,2),(9,2),(10,2),(11,2),(12,2),(13,2),(14,2),(15,3),(16,3),(17,3),(18,3),(19,3),(20,3),(21,4),(22,4),(23,4),(24,4),(25,4),(26,5),(27,5),(28,5),(29,6),(30,6),(31,6),(32,6),(33,6),(34,6),(35,6),(36,6),(37,6),(38,6),(39,6),(40,6),(41,6),(42,6),(132,7),(133,7),(134,7),(135,7),(132,8),(133,8),(134,8),(135,8),(43,9),(44,10),(45,10),(46,11),(47,12),(48,12),(49,12),(50,12),(136,13),(137,13),(138,13),(139,13),(140,13),(136,14),(137,14),(138,14),(139,14),(140,14),(51,15),(52,15),(53,15),(54,15),(55,15),(56,15),(57,15),(58,16),(59,16),(60,16),(61,16),(62,16),(63,17),(64,17),(65,17),(66,17),(67,18),(68,19),(69,19),(70,19),(71,19),(72,20),(73,20),(74,20),(75,20),(76,20),(77,20),(78,21),(79,21),(80,21),(81,21),(82,21),(83,21),(84,21),(85,21),(86,21),(87,21),(88,21),(89,21),(90,21),(91,21),(92,21),(93,21),(94,21),(95,21),(96,21),(97,22),(98,22),(99,22),(100,23),(101,24),(102,24),(103,25),(104,26),(105,26),(106,26),(107,26),(108,26),(109,26),(141,26),(141,27),(110,28),(111,28),(112,29),(113,29),(114,29),(115,29),(116,29),(117,30),(118,31),(119,32),(120,32),(121,32),(122,32),(123,32),(124,32),(125,32),(126,32),(127,32),(128,32),(129,32),(130,32),(131,32),(142,36);

SET FOREIGN_KEY_CHECKS = 1;