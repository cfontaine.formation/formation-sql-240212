USE bibliotheque;

-- Déclaration d'un procedure stockée
DELIMITER $	-- CHANGE le délimiteur de fin de ligne ; -> $
CREATE PROCEDURE auteur_vivant()
BEGIN
	SELECT prenom, nom,naissance FROM auteurs WHERE deces IS NULL;
END $ 
DELIMITER ;	-- CHANGE le délimiteur de fin de ligne $ -> ;

DELIMITER $
CREATE PROCEDURE test_variable()
BEGIN 
	-- Déclarer une variable locale
	-- DECLARE var_local INT; -- Valeur contenu dans la variable  -> NULL
	DECLARE var_local INT DEFAULT 42;
	SELECT var_local;
	-- Affecter une valeur à une variable locale
	SET var_local=2;
	SELECT var_local;
	-- INTO -> affecter une variable avec le résultat d'une requête
	SELECT count(id) INTO var_local FROM auteurs;
	SELECT var_local;
	SELECT @var_global;
	SET @var_global = var_local;
END $
DELIMITER ;

-- Passage de paramètre
-- - en entrée: 2 entiers a et b
-- - en sortie: un entier somme
DELIMITER $
CREATE PROCEDURE addition(IN a INT,IN b INT, OUT somme INT)
BEGIN
	SET somme=a+b;
END $
DELIMITER ;

-- nb_livre_genre -> la procédure stockée prend en paramètre l'id du genre 
-- et en sortie on obtient le nombre de livre qui correspond à ce genre 
DELIMITER $
CREATE PROCEDURE nb_livre_genre(IN nom_genre VARCHAR(50), OUT nb_livre INT)
BEGIN
	SELECT count(livres.id) INTO nb_livre FROM livres
	INNER JOIN genres ON livres.genre= genres.id
	WHERE genres.nom=nom_genre;
END $
DELIMITER ;

-- Exercice Procédure Stockée
-- Créer une procédure qui a pour nom: nb_ville_pays en paramètre, on a:
--  - en entrée un entier code_country
--  - en sortie un entier nb_ville_pays
-- La procédure pour le code_country passé en paramètre va retourner le nombre de ville
USE world;

DELIMITER $
CREATE PROCEDURE nb_ville_pays(IN code_country CHAR(3),OUT nb_ville_pays INT)
BEGIN
	SELECT count(id) INTO nb_ville_pays FROM city WHERE STRCMP(city.countrycode,code_country COLLATE utf8mb4_general_ci)=0;
END $
DELIMITER ;

USE bibliotheque;

-- Condition IF
-- La procédure prend en paramètre un entier et on retourne une chaine de caractère qui contient
-- paire ou impaire suivant la parité du paramètre d'entrée 
DELIMITER $
CREATE PROCEDURE parite(IN val INT, OUT str VARCHAR(10))
BEGIN
	IF val MOD 2=0 THEN 
		SET str='Paire';
	ELSE
		SET str='Impaire';
	END IF;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE nb_livre_genre2(IN nom_genre VARCHAR(50), OUT nb_livre INT)
BEGIN
	DECLARE id_genre INT;
	SELECT id INTO id_genre FROM genres WHERE nom=nom_genre;
	IF id_genre IS NULL THEN
		SET nb_livre=NULL;
	ELSE
		SELECT count(livres.id) INTO nb_livre FROM livres
		WHERE livres.genre=id_genre;
	END IF;
END $
DELIMITER ;

-- Condition CASE
-- La procédure prend en paramètre un entier et on retourne une chaine de caractère qui contient
-- la catégorie et l'age du livre
DELIMITER $
CREATE PROCEDURE categorie_age_livre(IN annee INT, OUT categorie_age VARCHAR(50))
BEGIN
	DECLARE age INT;
	IF annee<YEAR(current_date()) THEN
		SET age=YEAR(current_date())-annee;
		
		CASE
			WHEN annee >2000 THEN SET categorie_age=CONCAT('MODERNE (',age,' ans)');
			WHEN annee BETWEEN 1900 AND 2000 THEN SET categorie_age=CONCAT('20 ème siècle(',age,' ans)');
		ELSE 
			SET categorie_age=CONCAT('ANCIEN (',age,' ans)');
		END CASE;
	
	ELSE
		SET categorie_age=NULL;
	END IF;
END $
DELIMITER ;

-- Boucle LOOP
DELIMITER $
CREATE PROCEDURE somme_multi_loop(IN valeur INT,OUT resultat INT)
BEGIN 
	DECLARE i INT DEFAULT 0;
	SET resultat=0;
exit_loop: LOOP
		-- instructions
		SET i=i+1;
		SET resultat=resultat+i;
		IF i>=valeur THEN # pour quitter la boucle
			LEAVE exit_loop;
		END IF;
		-- instructions
	END LOOP;
END $
DELIMITER ;

-- Boucle while
DELIMITER $
CREATE PROCEDURE somme_multi_while(IN valeur INT,OUT resultat INT)
BEGIN 
	DECLARE i INT DEFAULT 0;
	SET resultat=0;
	WHILE i<valeur DO
		SET i=i+1;
		SET resultat=resultat+i;
	END WHILE;	
END $
DELIMITER ;

-- Boucle repeat
DELIMITER $
CREATE PROCEDURE somme_multi_repeat(IN valeur INT,OUT resultat INT)
BEGIN 
	DECLARE i INT DEFAULT 0;
	SET resultat=0;
	REPEAT 
		SET i=i+1;
		SET resultat=resultat+i;
	UNTIL i>=valeur END REPEAT;

END $
DELIMITER ;

-- Utilisation d'un curseur pour parcourir un résultat
DELIMITER $
CREATE PROCEDURE auteurs_by_livre(IN id_livre INT, OUT auteurs VARCHAR(500))
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE a VARCHAR(100);

	-- Déclaration du curseur 
	DECLARE cur_auteur CURSOR FOR SELECT CONCAT(prenom,' ',nom) FROM livre2auteur
	INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur 
	WHERE livre2auteur.id_livre=id_livre;
	
	-- Lorsque le curseur arrive en fin du résultat  la variable done est affectée à 1
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done=1;

	-- Ouverture du curseur
	OPEN cur_auteur;
	SET auteurs='';
	WHILE done=0 DO
		FETCH cur_auteur INTO a;	-- le nom de l'auteur est placé dans a variable auth 
									-- le curseur passe au prochain résultat
		IF done=0 THEN
			IF auteurs<>'' THEN 
				SET auteurs=CONCAT_WS(', ',auteurs,a);
			ELSE 
				SET auteurs=a;
			END IF;
		END IF;
	END WHILE;
	
	-- Fermeture du curseur
	CLOSE cur_auteur;
END $
DELIMITER ;

