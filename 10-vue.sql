USE bibliotheque;

-- Création d'une vue
CREATE VIEW v_livre_genre AS
SELECT livres.id,titre,annee, genres.nom AS genre FROM livres
INNER JOIN genres ON livres.genre=genres.id;

-- Création d'une vue
SHOW TABLES;

-- On peut exécuter des requêtes sur la vue
SELECT * FROM v_livre_genre;

SELECT * FROM v_livre_genre WHERE annee BETWEEN 1970 AND 2000
ORDER BY annee;


-- Si on ajoute des données dans la vue
INSERT INTO livres (titre,annee,genre) VALUES
('Les misérables',1862,7);

INSERT INTO livre2auteur (id_auteur,id_livre) VALUES 
(33,143);

-- Elle mise à jour dynamiquement
SELECT * FROM v_livre_genre WHERE genre='drame';

-- Supression de la vue
DROP VIEW v_livre_genre ;

CREATE VIEW v_auteur_livre AS 
SELECT livres.id,titre,annee,concat_ws(' ',prenom,nom) FROM livres
INNER JOIN livre2auteur ON livre2auteur.id_livre=livres.id 
INNER JOIN auteurs ON livre2auteur.id_auteur = auteurs.id;

SELECT * FROM v_auteur_livre;

-- ALTER VIEW -> Modifier une vue (recréation)
ALTER VIEW v_auteur_livre AS 
SELECT livres.id,titre,annee,genres.nom,concat(prenom,' ',auteurs.nom,'(',pays.nom,')') AS auteur FROM livres
INNER JOIN livre2auteur ON livre2auteur.id_livre=livres.id 
INNER JOIN auteurs ON livre2auteur.id_auteur = auteurs.id
INNER JOIN genres ON livres.genre = genres.id
INNER JOIN pays ON pays.id=auteurs.nation;

SELECT * FROM v_auteur_livre;
