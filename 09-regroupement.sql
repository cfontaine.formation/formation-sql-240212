USE bibliotheque;

-- Regroupement avec Group By
-- Obtenir le nombre de livre par année
SELECT annee,count(annee) FROM livres GROUP BY annee;

-- Obtenir le nombre de livre par année si il est supérieur ou égal à 3
SELECT annee, count(annee) AS nb_livre FROM livres WHERE genre=1 GROUP BY annee HAVING nb_livre>=3;

-- Obtenir le nombre de livre par genre
-- having -> condition (idem where) après le regroupement, Where se trouve toujours avant le regroupement
SELECT genres.nom, count(livres.id) AS nb_livre FROM livres
INNER JOIN genres ON genres.id=livres.genre
GROUP BY livres.genre HAVING  nb_livre >=5 ORDER BY nb_livre DESC;

-- Obtenir le nombre de livre par auteur
SELECT CONCAT_WS(' ',a.prenom,a.nom) AS auteur,count(l.id) AS nb_livre FROM auteurs a
INNER JOIN livre2auteur la ON a.id=la.id_auteur 
INNER JOIN livres l ON l.id=la.id_livre 
GROUP BY a.id ORDER BY nb_livre DESC, a.nom;

-- Obtenir le nombre de livre par genre pour le livres sortie après 1960 
-- pour les auteurs qui ont écrit au moins 10 livres
SELECT genres.nom,count(livres.genre) AS nb_livre FROM livres
INNER JOIN genres ON livres.genre=genres.id 
WHERE annee>1960
GROUP BY genre
HAVING nb_livre>10;

-- Exercice :Regroupement Group By
USE world;

--  Nombre de pays par continent
SELECT continent,count(code) AS nb_pays FROM country GROUP BY continent;

-- Afficher les continents et leur la population totale classé du plus peuplé au moins peuplé
SELECT continent, sum(population) AS population_totale FROM country
GROUP BY continent ORDER BY population_totale DESC;

-- Nombre de langue officielle par pays - classé par nombre de langue officielle
SELECT country.name, count(countrycode) AS nb_language FROM country 
INNER JOIN countrylanguage ON country.code=countrylanguage.CountryCode 
WHERE isofficial='T'
GROUP BY country.code ORDER BY nb_language, name; 

USE elevage;
-- Quelles sont les races dont nous ne possédons aucun individu
SELECT r.nom ,a.nom FROM race r 
LEFT JOIN animal a ON a.race_id =r.id;
GROUP BY r.nom
HAVING count(a.id)=0;

-- Fonction de fenetrage
USE exemple;

CREATE TABLE ventes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom_vendeur VARCHAR(30),
	annee INT,
	vente double
);

INSERT INTO ventes(nom_vendeur,annee,vente)
VALUES 
('Paul',2020,10000),
('Patrick',2020,11000),
('Paola',2020,15000),
('Paul',2021,8000),
('Patrick',2021,10000),
('Paola',2021,5000),
('Paul',2022,14000),
('Patrick',2022,5000),
('Paola',2022,16000),
('Paul',2019,12000),
('Patrick',2019,10000),
('Paola',2019,4000),
('Paul',2018,8000),
('Patrick',2018,10000),
('Paola',2018,5000),
('Paul',2017,1000),
('Patrick',2017,5000),
('Paola',2017,6000);

-- Clause over
-- OVER() -> la partition est la table complète
SELECT annee, nom_vendeur, vente, sum(vente) OVER() AS somme_vente  FROM ventes;

-- regroupement sur les années
-- avec un group by
SELECT annee,  sum(vente)  AS somme_vente   FROM ventes GROUP BY annee;

-- avec OVER(PARTITION BY )
-- OVER(PARTITION BY annee) -> les partitions correspondent aux annees
SELECT annee, nom_vendeur ,vente , sum(vente) OVER(PARTITION BY annee) AS somme_vente FROM ventes;

SELECT annee, nom_vendeur ,vente ,
sum(vente) OVER(PARTITION BY annee) AS somme_vente_annee,
sum(vente) OVER() AS somme_total,
round(avg(vente) OVER(PARTITION  BY annee),2) AS moyenne_vente_annee
FROM ventes;

-- OVER(PARTITION BY nom_vendeur) -> les partitions correspondent à nom_vendeur
SELECT annee, nom_vendeur ,vente ,
sum(vente) OVER(PARTITION BY nom_vendeur) FROM ventes;

SELECT annee, nom_vendeur ,vente ,
sum(vente) OVER(PARTITION BY nom_vendeur ORDER BY vente,annee DESC) FROM ventes;


-- Fonction de fenetrage
-- 1 Fonction d'agrégation: Min,Max,Sum,Avg,Count
SELECT annee, nom_vendeur ,vente ,
sum(vente) OVER(PARTITION BY nom_vendeur) FROM ventes;

-- 2 Fonction de fenetrage de classement, il faut utiliser ORDER BY
-- ROW_NUMBER() -> numérote 1, 2, ... les lignes de la partitions
SELECT annee,nom_vendeur ,vente,
ROW_NUMBER () OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

SELECT annee,nom_vendeur ,vente,
ROW_NUMBER () OVER(PARTITION BY annee ORDER BY vente DESC ,nom_vendeur) FROM ventes;

-- RANK() -> Attribue un rang à chaque ligne de sa partition en fonction de la clause ORDER BY,
--           mais on aura des écarts dans la séquence des valeurs lorsque plusieurs lignes ont le même rang
-- 1 - 2 - 3 - 4 - 4 - 6

SELECT annee,nom_vendeur ,vente,
RANK () OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- DENSE_RANK() -> Attribue un rang à chaque ligne de sa partition en fonction de la clause ORDER BY
-- 1 - 2 - 3 - 4 - 4 - 5
SELECT annee,nom_vendeur ,vente,
DENSE_RANK () OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- PERCENT_RANK() -> Calculer le rang en pourcentage (rank - 1)/(rows - 1)
SELECT annee,nom_vendeur ,vente,
PERCENT_RANK () OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- NTILE(n)-> Distribue les lignes de chaque partition de fenêtre dans un nombre spécifié de groupes clas
SELECT annee,nom_vendeur ,vente,
NTILE(3) OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- CUME_DIST -> Calcule la distribution cumulée d'une valeur dans un ensemble de valeurs
SELECT annee,nom_vendeur ,vente,
CUME_DIST()  OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

--  Fonction de fenetrage de valeur 
-- LAG(colonne) -> Renvoie la valeur de la ligne avant la ligne actuelle dans une partition
-- vente	lag(vente)
-- 16000
-- 15000	16000
SELECT annee,nom_vendeur ,vente,
LAG(vente)  OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

SELECT annee,nom_vendeur ,vente,
vente- LAG(vente)  OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- LEAD(colonne) -> Renvoie la valeur de la ligne après la ligne actuelle dans une partition
-- vente	lead(vente)
-- 16000	5000
-- 5000		15000
SELECT annee,nom_vendeur ,vente,
LEAD(vente)  OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- FIRST_VALUE(colonne) ->Renvoie la valeur de l'expression spécifiée par rapport à la première ligne de la frame
SELECT annee,nom_vendeur ,vente,
FIRST_VALUE(vente)  OVER(PARTITION BY nom_vendeur) FROM ventes;

-- LAST_VALUE(colonne) -> Renvoie la valeur de l'expression spécifiée par rapport à la dernière ligne de la frame
SELECT annee,nom_vendeur ,vente,
LAST_VALUE(vente)  OVER(PARTITION BY nom_vendeur) FROM ventes;

-- NTH_VALUE(colonne,num_ligne) -> Renvoie la valeur de l'argument de la nème ligne
SELECT annee,nom_vendeur ,vente,
NTH_VALUE(vente,5)  OVER(PARTITION BY nom_vendeur) FROM ventes;

-- Exercice fonction de fenêtrage 
USE world;

-- Afficher le nom du pays , le nom du continent , la population du pays,la population total du continent
SELECT name,continent,population,
sum(population) OVER(PARTITION BY continent) AS continent_population 
FROM country;

-- Ajouter une colonne qui affiche le classement du pays par continent en fonction de la population du pays décroissante
SELECT name,continent,population,
sum(population) OVER(PARTITION BY continent) AS continent_population,
DENSE_RANK() OVER(PARTITION BY continent ORDER BY population DESC) AS classement 
FROM country;

-- FRAME
-- Frame unit -->ROWS

-- Frame start
-- UNBOUNDED PRECEDING
SELECT annee,nom_vendeur ,vente,
sum(vente) Over(PARTITION BY nom_vendeur
				ORDER BY annee
				ROWS UNBOUNDED PRECEDING)
FROM ventes;

-- N PRECEDING
SELECT annee,nom_vendeur ,vente,
sum(vente) Over(PARTITION BY nom_vendeur
				ORDER BY annee
				ROWS 2 PRECEDING)
FROM ventes;

-- Frame between
SELECT annee,nom_vendeur ,vente,
sum(vente) Over(PARTITION BY nom_vendeur
				ORDER BY annee
				ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING 
				)
FROM ventes;

SELECT annee,nom_vendeur ,vente,
sum(vente) Over(PARTITION BY nom_vendeur
				ORDER BY annee
				ROWS BETWEEN CURRENT ROW AND 1 FOLLOWING 
				)
FROM ventes;

SELECT annee,nom_vendeur ,vente,
sum(vente) Over(PARTITION BY nom_vendeur
				ORDER BY annee
				ROWS BETWEEN UNBOUNDED PRECEDING AND 1 FOLLOWING 
				)
FROM ventes;

-- Frame unit RANGE
SELECT annee,nom_vendeur ,vente,
sum(vente) Over(PARTITION BY nom_vendeur
				ORDER BY vente
				RANGE UNBOUNDED PRECEDING)
FROM ventes;

SELECT annee,nom_vendeur ,vente,
sum(vente) Over(PARTITION BY nom_vendeur
				ORDER BY vente
				RANGE 1 PRECEDING)
FROM ventes;


-- GROUP BY WITH ROLLUP
-- La clause ROLLUP génère plusieurs ensembles de regroupement en fonction 
-- des colonnes ou des expressions spécifiées dans la clause GROUP BY
USE exemple;
SELECT COALESCE (annee,'Total=') AS annees, SUM(vente) AS somme_vente FROM ventes 
GROUP BY annee WITH ROLLUP;

USE bibliotheque;
SELECT COALESCE ( genres.nom,'Total='), count(livres.id) AS nb_livres FROM livres 
INNER JOIN genres ON genres.id=livres.genre
GROUP BY genres.nom WITH ROLLUP ;

-- GROUPING() NULL -> 1 sinon 0
SELECT IF(GROUPING(pays.nom)=0,pays.nom,'Total='),count(auteurs.id),grouping(pays.nom) FROM auteurs
INNER JOIN pays ON auteurs.nation = pays.id 
GROUP BY pays.nom WITH ROLLUP;

USE exemple;
SELECT 
IF (GROUPING(annee)=0,annee,'Total='),
IF(GROUPING(nom_vendeur)=0,nom_vendeur ,'Sous-total='),
sum(vente),GROUPING(annee),GROUPING(nom_vendeur)
FROM ventes
GROUP BY annee,nom_vendeur WITH ROLLUP;

-- Exercice: Group By avec WITH ROLLUP
-- Combien de mâles et de femelles de chaque race avons-nous, avec un compte total intermédiaire pour les races (mâles et femelles confondues)
USE elevage;
SELECT 	IF(GROUPING(animal.sexe)=0,animal.sexe,IF(GROUPING(race.nom)=0,'sous-total','')),
 		IF(GROUPING(race.nom)=0,race.nom, 'total=')
 		,count(animal.id) FROM animal
INNER JOIN race ON animal.race_id = race.id 
GROUP BY race.nom,animal.sexe WITH ROLLUP;
