USE exemple;

CREATE TABLE compte_bancaire(
	id INT PRIMARY KEY AUTO_INCREMENT,
	solde DECIMAL(10,3),
	iban VARCHAR(20),
	titulaire VARCHAR(50)
);

INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES
(1000.000,'FR-105-0000-01','John Doe'),
(5000.000,'FR-105-0000-02','Jane Doe'),
(500.000,'FR-105-0000-03','Alan Smithee'),
(400.000,'FR-105-0000-04','Yves Roulo'),
(4000.000,'FR-105-0000-05','Jo Dalton');

-- Atomicité
-- désactiver l'autocommit
SET autocommit=0;

START TRANSACTION; -- Commencer une transaction
-- transfert entre 2 comptes
UPDATE compte_bancaire SET solde=solde-100.0 WHERE id=2;
UPDATE compte_bancaire SET solde=solde+100.0 WHERE id=4;
COMMIT; -- Valider la transsaction

SELECT * FROM compte_bancaire;

START TRANSACTION; -- Commencer une transaction
-- transfert entre 2 comptes
UPDATE compte_bancaire SET solde=solde-1000.0 WHERE id=2;
SELECT * FROM compte_bancaire;
ROLLBACK; -- annulation de la transaction

SELECT * FROM compte_bancaire;

SET autocommit=1;

-- Exemple point de sauvagarde
START TRANSACTION;
UPDATE compte_bancaire SET solde=solde-20.0 WHERE id=1;
UPDATE compte_bancaire SET solde=solde+20.0 WHERE id=3;
SAVEPOINT transfert1;
INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES 
(6000.0,'FR-1000-105','Antoine Beretto');
ROLLBACK TO transfert1;
INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES 
(600.0,'FR-1000-105','Antoine Beretto');
SAVEPOINT creation_compte_beretto;
UPDATE compte_bancaire SET solde=solde-60.0 WHERE id=7;
UPDATE compte_bancaire SET solde=solde+60.0 WHERE id=3;

-- Liberer les points de sauvegarde
RELEASE SAVEPOINT creation_compte_beretto;
RELEASE SAVEPOINT transfert1;
COMMIT;

SELECT * FROM compte_bancaire;

SET autocommit=1;

CREATE USER 'user1'@'localhost' IDENTIFIED BY 'dawan';

-- GRANT droit ON base_de_donnees.table TO utilisateur WITH GRANT OPTION

-- droit: CREATE , ALTER, DROP, INSERT, UPDATE, DELETE, RFERENCES, FLUSH, RELOAD
-- pour base_de_donnees et table -> on peut utiliser un wildcard *
-- WITH GRANT OPTION : permet à cet utilisateur d'accorder toutes les autorisations dont il dispose à d'autre utilisateur

GRANT ALL ON exemple.compte_bancaire TO 'user1'@'localhost' WITH GRANT OPTION;

FLUSH PRIVILEGES;

SHOW GRANTS FOR 'user1'@'localhost';

START TRANSACTION; -- Commencer une transaction
				   -- transfert entre 2 comptes
UPDATE compte_bancaire SET solde=solde-300.0 WHERE id=5;
UPDATE compte_bancaire SET solde=solde+300.0 WHERE id=4;
COMMIT;

DROP USER 'user1'@'localhost';