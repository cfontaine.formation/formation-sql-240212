USE bibliotheque;

-- Création d'un déclencheur
DELIMITER $
CREATE TRIGGER valide_genre	
BEFORE INSERT	-- le déclencheur va être exécuté avant chaque INSERT sur la TABLE genres
ON genres
FOR EACH ROW 	-- Le déclencheur sera éxécuté pour chaque ligne
BEGIN
	DECLARE id_genre INT;
	SELECT genres.id INTO id_genre FROM genres WHERE nom=NEW.nom;
	-- L'insert sera refusé si le nombre de caractère est <3 et >30
	IF char_length(NEW.nom)<3 OR char_length(NEW.nom)>30 THEN 
		-- SQLSTATE -> https://en.wikipedia.org/wiki/SQLSTATE
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = " nom du genre non conforme.";
	END IF ;
	-- L'insert sera refusé si le nom du genre existe déjà
	IF id_genre IS NOT NULL THEN 
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = "nom du genre existe déjà.";
	END IF ;
END $
DELIMITER ;


-- Déclencheur
-- INSERT INTO genres(nom) VALUES("policier"); -- -> Erreur: le nom du genre existe déjà
-- INSERT INTO genres(nom) VALUES('a'); -- -> Erreur: le nom du genre est < à 3 caratères
INSERT INTO genres(nom) VALUES('Théatre'); -- OK 
-- INSERT INTO genres(nom) VALUES('Théatre'); -> Erreur: le nom du genre existe déjà


-- Supression du déclencheur
DROP TRIGGER valide_genre;
