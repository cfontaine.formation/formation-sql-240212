USE exemple;

-- Ajouter des données
-- Insérer une ligne en spécifiant toutes les colonnes
INSERT INTO marques VALUES (1,'Marque A','1987-03-07');
INSERT INTO marques VALUES (2,'Marque B','1997-07-17');

-- Insérer une ligne en spécifiant les colonnes souhaitées
INSERT INTO marques(nom,date_creation) VALUES('Marque C', '1904-01-20');
INSERT INTO marques(nom) VALUES('Marque D');

-- Erreur -> nom ne peut pas être NULL
-- INSERT INTO marques(date_creation) VALUES('1904-01-20');

-- Insérer Plusieurs lignes
INSERT INTO articles(reference ,prix,description_article,marque) VALUES 
(100,550.0,'TV',2),
(4000,50.0,'Souris gaming',1),
(4001,19.0,'Clavier azerty',1),
(1000,150.0,'Disque dur SSD 2GO',NULL),
(103,950.0,'TV 4K',2);

-- Insérer Plusieurs lignes
-- Erreur -> La marque avec l'id 10 n'existe pas, la contrainte d'intégritée référentielle (fk_articles_marques)
-- , n'est pas respecté
-- INSERT INTO articles(reference ,prix,description_article,marque) VALUES (107,550.0,'TV 4K',10);

-- Insérer des données dans une relation n-n
INSERT INTO fournisseurs (nom) VALUES 
('fournisseur 1'),
('fournisseur 2');

INSERT INTO fournisseurs_articles(id_article,id_fournisseur) VALUES 
(100,1),
(100,2),
(4000,1),
(4001,1),
(1000,2);

-- Supprimer des données => DELETE, TRUNCATE
-- Supression de la ligne qui a pour id 4 dans la table marques
DELETE FROM marques WHERE id=4;

-- Erreur -> on ne peut pas supprimer la marque id=1, car il ya des articles qui font référence à cette marque
-- DELETE FROM marques WHERE id=1;

-- Supprimer toutes les lignes de la table qui ont un prix > 600.0
DELETE FROM articles WHERE prix>600.0;

-- Supprimer toutes les lignes de la table fournisseurs_articles (il n'y a pas de condition)
DELETE FROM fournisseurs_articles;

-- Valeur auto_increment et la suppression
-- DELETE => ne ré-initialise pas la colonne AUTO_INCREMENT
DELETE FROM fournisseurs; 

INSERT INTO fournisseurs (nom) VALUES ('fournisseur 1');

-- TRUNCATE => réinitailise l'auto incrément
SET FOREIGN_KEY_CHECKS=0;	-- désactiver la vérification des clés étrangères (MYSQL/ MARIADB)
-- Supression de toutes les lignes de la table articles  => remet la colonne AUTO_INCREMENT à 0
TRUNCATE TABLE fournisseurs;
SET FOREIGN_KEY_CHECKS=1;	-- réactiver la vérification des clés étrangères

INSERT INTO fournisseurs (nom) VALUES ('fournisseur 1');

-- Modification des données => UPDATE
-- Modification du prix et de la description de l'article qui a pour reference 100
UPDATE articles SET prix=600.0, description_article='TV 100hz' WHERE reference = 100; 

-- Modification du prix à 10.0 des articles qui ont un prix inférieur à 40.0 
UPDATE articles SET prix=10.0 WHERE prix<40.0;

-- Modifier tous les prix supérieur à 300.0 en les augmentant de 20%
UPDATE articles SET prix=prix*1.20 WHERE prix>300.0;

-- Pas de condition -> tous les prix passe à 0.0
UPDATE articles SET prix=0.0;