-- commentaire fin de ligne

# commentaire fin de ligne

/*
	commentaires sur
	plusieurs 
	lignes
*/

-- Créer un base de données exemple
CREATE DATABASE exemple;

-- Afficher l'ensemble des bases de données 
-- SHOW DATABASES;

-- Choisir la base de données exemple, comme base courante
USE exemple;

-- Créer une table article
CREATE TABLE article(
	reference INT,
	prix DECIMAL(6,2) COMMENT 'prix de l''article',
	description VARCHAR(255)
) ENGINE=InnoDB, COLLATE=utf8mb4_general_ci;

-- Supprimer la table article
-- DROP TABLE article ;

-- Lister les tables de la base de données
SHOW TABLES;

-- Afficher la description de la table (mysql)
DESCRIBE article;

-- Afficher la description de la table avec les commentaire (mysql)
SHOW FULL COLUMNS FROM articles;

-- Exercice: créer la table vols
CREATE TABLE vols(
	numero_vol INT,
	heure_depart DATETIME,
	ville_depart VARCHAR(150),
	heure_arrivee DATETIME,
	ville_arrivee VARCHAR(150)
) ENGINE=InnoDB, COLLATE=utf8mb4_general_ci;


-- Modifier une table -> ALTER TABLE
-- renommer une table (mysql/mariadb)
RENAME TABLE article TO articles;

-- Ajouter une colonne
ALTER TABLE articles ADD nom VARCHAR(20);

-- Supprimer une colonne
ALTER TABLE articles DROP nom;

-- Modifier le type d'une colonne	VARCHAR(255) -> VARCHAR(300)
ALTER TABLE articles MODIFY description VARCHAR(300);

-- Modifier le nom d'une colonne (description -> description_article)
ALTER TABLE articles CHANGE description description_article VARCHAR(300);

-- Colonne nullable
-- à la création de la table
/*
CREATE TABLE articles(
	reference INT,
	prix DECIMAL(6,2) NOT NULL,
	description_article VARCHAR(300)
);
*/
-- Ajouter une NOT NULL sur colonne existante
ALTER TABLE articles MODIFY prix DECIMAL(6,2) NOT NULL; 

-- Valeur par défaut
-- à la création de la table
/*
CREATE TABLE articles(
	reference INT,
	prix DECIMAL(6,2) NOT NULL,
	description_article VARCHAR(300) DEFAULT 'sans'
);
*/


-- Modifier une colonne pour ajouter une valeur par défaut 'sans' pour la description de l'article
ALTER TABLE articles MODIFY description_article VARCHAR(300) DEFAULT 'sans';

-- Contrainte d’unicité
-- à la création de la table
/* CREATE TABLE stagiaires(
    id INT ,
 	prenom VARCHAR(50),
 	nom  VARCHAR(50) NOT NULL,
 	email VARCHAR(200) NOT NULL UNIQUE
 );*/

-- Ajouter une contrainte d'unicité
-- ALTER TABLE stagiaires ADD UNIQUE(email);

-- Ajouter une contrainte nommé d'unicité 
ALTER TABLE stagiaires ADD CONSTRAINT un_email_stagiaires UNIQUE (email);

-- Supprimer une contrainte nommé d'unicité
-- ALTER TABLE stagiaires DROP CONSTRAINT un_email_stagiaires;

-- Clé primaire
-- Une clé primaire est la donnée qui permet d'identifier de manière unique une ligne dans une table
-- à la création de la table
/*
CREATE TABLE articles(
	reference INT PRIMARY KEY,
	prix DECIMAL(6,2) NOT NULL,
	description_article VARCHAR(300) DEFAULT 'sans'
);
*/

-- Modifier la table articles pour que la colonne reférence deviennent la clé primaire de la table
ALTER TABLE articles ADD CONSTRAINT pk_articles PRIMARY KEY(reference);

CREATE TABLE formation(
	nom VARCHAR(80) NOT NULL,
	prix DECIMAL(6,2) NOT NULL
)

ALTER TABLE formation ADD id INT;

ALTER TABLE formation ADD CONSTRAINT pk_formation PRIMARY KEY(id);

-- auto-increment
-- à la création de la table
 CREATE TABLE stagiaires(
    id INT PRIMARY KEY AUTO_INCREMENT,
 	prenom VARCHAR(50),
 	nom  VARCHAR(50) NOT NULL,
 	email VARCHAR(200) NOT NULL UNIQUE
 );

-- Modifier la colonne référence pour ajouter AUTO_INCREMENT sur la clé primaire
ALTER TABLE stagiaires MODIFY id INT AUTO_INCREMENT;

-- Modifier la valeur initiale de l'auto increment à 100 par défaut à 1 
ALTER TABLE stagiaires AUTO_INCREMENT=100;

-- Exercice table pilotes
CREATE TABLE pilotes 
(
	numero_pilote INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50) NOT NULL,
	nom VARCHAR(50) NOT NULL,
	nombre_heure_vol INT NOT NULL
);

-- Exercice table avions
CREATE TABLE avions
(
	numero_avion INT PRIMARY KEY AUTO_INCREMENT,
	modele VARCHAR(60) NOT NULL,
	capacite SMALLINT NOT NULL
)

-- Exercice table vols
-- Modifier la table vols pour que la colonne numero_vol deviennent la clé primaire de la table
ALTER TABLE vols ADD CONSTRAINT plk_vols PRIMARY KEY(numero_vol);

-- Ajouter AUTO_INCREMENT sur la clé primaire numero_vol
ALTER TABLE vols MODIFY numero_vol INT AUTO_INCREMENT;

-- Ajouter NOT NULL sur les colonnes ville_depart et ville_arrive
ALTER TABLE vols MODIFY ville_depart VARCHAR(150) NOT NULL;
ALTER TABLE vols MODIFY ville_arrivee VARCHAR(150) NOT NULL;

-- Relations entre tables

-- Relation 1,n
CREATE TABLE marques
(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL,
	date_creation DATE
);

-- On ajoute une colonne qui sera la clé étrangère
ALTER TABLE articles ADD marque INT;

-- Ajout d'une contrainte de clé étrangère 
-- clé étrangère colonne marque de article
-- elle fait référence à la clé primaire id de la table marques
ALTER TABLE articles ADD 
CONSTRAINT fk_articles_marques
FOREIGN KEY(marque)
REFERENCES marques(id);


-- Création de la table articles directement avec la contrainte de clé étrangère
/*
CREATE TABLE articles(
	reference INT PRIMARY KEY,
	prix DECIMAL(6,2) NOT NULL,
	description_article VARCHAR(300) DEFAULT 'sans'
	marque INT,
	
	CONSTRAINT  fk_articles_marques
	FOREIGN KEY(marque)
	REFERENCES marques(id)
);
*/

-- Exercice: Faire la relation entre vols et pilote
ALTER TABLE vols ADD pilote INT;

ALTER TABLE vols ADD CONSTRAINT fk_vols_pilotes
FOREIGN KEY (pilote)
REFERENCES pilotes(numero_pilote);

ALTER TABLE vols ADD avion INT;

ALTER TABLE vols ADD CONSTRAINT fk_vols_avions
FOREIGN KEY (avion)
REFERENCES avions(numero_avion);

-- Relation n-n
CREATE TABLE fournisseurs
(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL
);

-- création table de jonction
CREATE TABLE fournisseurs_articles
(
	id_article INT,
	id_fournisseur INT,
	
	CONSTRAINT fk_fournisseurs_articles
	FOREIGN KEY (id_fournisseur)
	REFERENCES fournisseurs(id),
	
	CONSTRAINT fk_articles_fournisseurs
	FOREIGN KEY (id_article)
	REFERENCES articles(reference),
	
	-- clé primaire composée id_article et id_fournisseur
	CONSTRAINT pk_fournisseurs_articles
	PRIMARY KEY(id_article,id_fournisseur)
);
